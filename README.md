# dotnet-container-app

Sandbox project based on https://learn.microsoft.com/en-us/dotnet/core/docker/publish-as-container.

## Init

```bash
mkdir src
dotnet new worker -o src/Worker -n DotNet.ContainerImage
dotnet new solution
dotnet sln add src/Worker
dotnet add src/Worker package Microsoft.NET.Build.Containers
dotnet new gitignore
```

## Development

Local testing - modify `.csproj` before running command

```bash
dotnet publish --os linux --arch x64 /t:PublishContainer
```
